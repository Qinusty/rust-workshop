use std::env;
use std::fs;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let valid_chars = ['>', '<', '+', '-', '[', ']', '.', ','];

    let path = env::args().nth(1).ok_or("A file path must be provided.")?;
    let program: String = fs::read_to_string(&path)?
        .chars()
        .filter(|c| valid_chars.contains(c))
        .collect();
    print!("{}", program);

    Ok(())
}
