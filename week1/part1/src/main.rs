use std::env;
use std::io::{BufReader, BufRead};
use std::fs::File;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    
    if let Some(path) = env::args().skip(1).next() {
        let f = File::open(&path)?;
        let f = BufReader::new(f);
        let lines = f.lines();


        // Couldn't quite get fold to play nice with ? And Result
        // let sum = lines.filter_map(Result::ok)
        //                .map(|line| line.parse::<i32>())
        //                .fold(Ok(0), |acc, x| acc + x?);

        let nums: Result<Vec<i32>, _> = lines.filter_map(Result::ok)
                                             .map(|line| line.parse::<i32>())
                                             .collect();

        let mut sum = 0;
        for num in nums? {
            sum += num;
        }
        print!("{}", sum);
    }
    Ok(())
}


