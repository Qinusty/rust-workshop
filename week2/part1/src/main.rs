use std::collections::HashMap;
use std::convert::TryFrom;
use std::env::args;
use std::fmt;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

use std::error;

#[derive(Debug)]
enum Person {
    NoNumber { name: String },
    WithNumber { name: String, number: isize },
}

impl TryFrom<&String> for Person {
    type Error = Box<dyn std::error::Error>;
    fn try_from(value: &String) -> Result<Self, Self::Error> {
        let mut parts = value.split(':');
        
        let name = parts.next().unwrap().to_string();
        let person = match parts.next() {
            Some(number) => Person::WithNumber {
                name,
                number: number.parse::<isize>()?,
            },
            _ => Person::NoNumber { name },
        };
        // println!("{:?}", person);
        Ok(person)
    }
}

impl Person {
    pub fn get_name(&self) -> String {
        match self {
            Person::NoNumber { name } => name.to_string(),
            Person::WithNumber {name, number: _ } => name.to_string()
        }
    }
}

#[derive(Default, Debug)]
struct ScoreSheet {
    total: isize,
    scores_counted: isize,
    people_scores: HashMap<String, PersonScore>,
}

#[derive(Default, Debug)]
struct PersonScore {
    tests_taken: usize,
    tests_missed: usize,
    total_score: isize
}

impl PersonScore {
    pub fn new(tests_taken: usize, tests_missed: usize, total_score: isize) -> PersonScore {
        PersonScore { tests_missed, tests_taken, total_score}
    }

    pub fn update(&mut self, score: isize, missed: bool) {
        if missed {
            self.tests_missed += 1;
        }
        else {
            self.tests_taken += 1;
        }
        self.total_score += score;
    }

    pub fn get_tests_taken(&self) -> usize { self.tests_taken }
    pub fn get_tests_missed(&self) -> usize { self.tests_missed }
    pub fn get_total_score(&self) -> isize { self.total_score }
}

impl ScoreSheet {
    pub fn add_score(&mut self, person: &Person) {
        use Person::*;

        match person {
            NoNumber { name } => self._add_score(&name, &0, true),
            WithNumber { name, number } => self._add_score(&name, &number, false),
        }
    }

    fn _add_score(&mut self, name: &String, score: &isize, missed: bool) {
        self.total += score;
        self.scores_counted += 1;
        self.people_scores
            .entry(name.to_string())
            .and_modify(|person_score| {
                person_score.update(*score, missed);
            })
            .or_insert(PersonScore::new(
                if missed { 0 } else { 1 },
                if missed { 1 } else { 0 },
                *score
            ));
    }

    pub fn get_map(&self) -> &HashMap<String, PersonScore>{
        &self.people_scores
    }

    pub fn get_score(&self, name: &String) -> &PersonScore {
        &self.people_scores[name]
    }
}

fn load_peoples(filename: &str) -> Result<Vec<Person>, Box<dyn std::error::Error>> {
    let f = File::open(&filename)?;
    let lines = BufReader::new(f).lines();
    let r: Result<Vec<Person>, Box<dyn std::error::Error>> =
        lines.map(|line| Person::try_from(&line.unwrap())).collect();
    r
}

#[derive(Debug, Clone)]
struct NotOk;

impl fmt::Display for NotOk {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "It wasn't Ok :(")
    }
}

// This is important for other errors to wrap this one.
impl error::Error for NotOk {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        // Generic error, underlying cause isn't tracked.
        None
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let filename = args().nth(1);
    let filename = filename.ok_or_else(|| NotOk)?;
    let peoples = load_peoples(&filename)?;
    let mut sheet: ScoreSheet = Default::default();
    for person in peoples {
        sheet.add_score(&person);
    }

    for (name, record) in sheet.get_map().iter() {
        println!("{} took {} tests, with a total score of {}. They missed {} tests.",
         name, record.get_tests_taken(), 
         record.get_total_score(), record.get_tests_missed()
        );
    }
    Ok(())
}
