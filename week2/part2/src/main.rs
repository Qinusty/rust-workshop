use std::env;
use std::fs;
use std::fmt;


type StandardResult<T> = Result<T, Box<dyn std::error::Error>>;

#[derive(Debug)]
enum RawInstruction {
    IncrementPtr,
    DecrementPtr,
    Increment,
    Decrement,
    Output,
    Input,
    StartLoop,
    EndLoop,
}


impl RawInstruction {
    fn from_char(val: char) -> Option<RawInstruction> {
        match val {
            '>' => Some(RawInstruction::IncrementPtr),
            '<' => Some(RawInstruction::DecrementPtr),
            '+' => Some(RawInstruction::Increment),
            '-' => Some(RawInstruction::Decrement),
            '.' => Some(RawInstruction::Output),
            ',' => Some(RawInstruction::Input),
            '[' => Some(RawInstruction::StartLoop),
            ']' => Some(RawInstruction::EndLoop),
            _ => None
        }
    }
} 

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
struct InputInstruction {
    instruction: RawInstruction,
    line: usize,
    column: usize,
    input_file: String
}

impl fmt::Display for InputInstruction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}:{}:{}] {}", 
            self.input_file, self.line, 
            self.column, self.instruction
        )
    }
}


fn parse_file(path: String) -> StandardResult<Vec<InputInstruction>> {
    let program_text = fs::read_to_string(&path)?;

    let mut row = 1;
    let mut col = 1;
    let mut instructions = Vec::new();

    for c in program_text.chars() {
        if c == '\n' {
            col = 1;
            row += 1;
        }
        else {
            if let Some(raw_instruction) = RawInstruction::from_char(c) {
                instructions.push(InputInstruction{
                instruction: raw_instruction,
                line: row,
                column: col,
                input_file: path.to_string()
                })
            }
            col += 1;
        }
    }
    Ok(instructions)
    
}


fn main() -> Result<(), Box<dyn std::error::Error>> {

    let path = env::args().nth(1).ok_or("A file path must be provided.")?;

    let instructions = parse_file(path);

    for instruction in instructions? {
        println!("{}", instruction);
    }

    Ok(())
}
